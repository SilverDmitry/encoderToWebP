package com.viled.encoder.service;

import com.viled.encoder.exception.EncoderException;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;

public interface EncoderService {
    String execute(Path inputPath, String formatName);
    String toWebP(Path inputPath) throws EncoderException;
    List<String> toWebP(List<Path> inputPath) throws EncoderException;
    String toWebP(String inputPath) throws EncoderException;
    byte[] execute(byte[] img, String filename, String formatName, boolean deleteLocalImg) throws URISyntaxException, IOException;
    BufferedImage toWebP(BufferedImage input, String dirName, String fileName, boolean deleteLocalImg);
    byte[] execute(byte[] input, String fileName, String formatName, boolean deleteLocalImg, float quality);
    Path execute(MultipartFile input, String formatName);
    List<Path> execute(MultipartFile[] input, String formatName);
}
