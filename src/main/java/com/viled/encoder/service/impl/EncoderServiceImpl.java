package com.viled.encoder.service.impl;

import com.luciad.imageio.webp.WebPWriteParam;
import com.viled.encoder.config.EncoderProperties;
import com.viled.encoder.exception.EncoderException;
import com.viled.encoder.service.EncoderService;
import com.viled.encoder.utils.Utils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EncoderServiceImpl implements EncoderService {
    final private String TEMP_DIR;
    final private float QUALITY;

    @Autowired
    public EncoderServiceImpl(EncoderProperties properties){
        this.TEMP_DIR = properties.getTempDir();
        this.QUALITY = properties.getQUALITY();
        try {
            Files.createDirectories(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize());
        } catch (Exception ex) {
            throw new EncoderException("Could not create temp directory.", ex);
        }
    }

    @Override
    public Path execute(MultipartFile input, String formatName) {
        String fileName = Utils.removeExtention(input.getOriginalFilename());
        Path pathWebP = Paths.get(TEMP_DIR + fileName + "." + formatName).toAbsolutePath().normalize();
        BufferedImage image = null;

        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));

            image = ImageIO.read(new ByteArrayInputStream(input.getBytes()));
            ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();

            WebPWriteParam writeParam = new WebPWriteParam(writer.getLocale());
            writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            writeParam.setCompressionType(writeParam.getCompressionTypes()[WebPWriteParam.LOSSY_COMPRESSION]);
            writeParam.setCompressionQuality(QUALITY);

            // Save the image
            writer.setOutput(new FileImageOutputStream(new File(pathWebP.toString())));
            writer.write(null, new IIOImage(image, null, null), writeParam);

        } catch (IOException e) {
            e.printStackTrace();
            new EncoderException("file "+ fileName +" did not encode to " + formatName, e);
        }
        return pathWebP;
    }

    @Override
    public List<Path> execute(MultipartFile[] input, String formatName){
        List<Path> pathImages = new ArrayList<>();

        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));

        for(int i=0; i< input.length; i++) {
            String fileName = Utils.removeExtention(input[i].getOriginalFilename());
            Path pathWebP = Paths.get(TEMP_DIR + fileName + "." + formatName).toAbsolutePath().normalize();
            pathImages.add(pathWebP);
            BufferedImage image = null;

            try {
                image = ImageIO.read(new ByteArrayInputStream(input[i].getBytes()));
                ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();

                WebPWriteParam writeParam = new WebPWriteParam(writer.getLocale());
                writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                writeParam.setCompressionType(writeParam.getCompressionTypes()[WebPWriteParam.LOSSY_COMPRESSION]);
                writeParam.setCompressionQuality(QUALITY);

                // Save the image
                writer.setOutput(new FileImageOutputStream(new File(pathWebP.toString())));
                writer.write(null, new IIOImage(image, null, null), writeParam);

            } catch (IOException e) {
                e.printStackTrace();
                new EncoderException("file " + fileName + " did not encode to " + formatName, e);
            }
        }
        } catch (IOException e) {
            new EncoderException("Did not create temp dir: "+ TEMP_DIR + " or deleted old files ", e);
        }
        return pathImages;
    }



    @Override
    public String execute(Path input, String formatName) {
        String pathWebP = Utils.removeExtention(input.toString()).concat("." + formatName);

        BufferedImage image = null;
        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));
            image = ImageIO.read(new File(input.toString()));
            boolean statusWrite = ImageIO.write(image, formatName, new File(pathWebP));
        } catch (IOException e) {
            e.printStackTrace();
            new EncoderException("file did not encode to " + formatName);
        }
        return pathWebP;
    }

    @Override
    public byte[] execute(byte[] input, String fileName, String formatName, boolean deleteLocalImg) {
        Path pathWebP = Paths.get(TEMP_DIR + fileName + "." + formatName).toAbsolutePath().normalize();
        byte[] imgWebp = null;
        BufferedImage image = null;

        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));
            image = ImageIO.read(new ByteArrayInputStream(input));
            boolean statusWrite = ImageIO.write(image, formatName, new File(pathWebP.toString()));
            imgWebp = Files.readAllBytes(pathWebP);

        } catch (IOException e) {
            e.printStackTrace();
            new EncoderException("file "+ fileName +" did not encode to " + formatName, e);
        }
        return imgWebp;
    }

    @Override
    public byte[] execute(byte[] input, String fileName, String formatName, boolean deleteLocalImg, float quality) {
        Path pathWebP = Paths.get(TEMP_DIR + fileName + "." + formatName).toAbsolutePath().normalize();
        byte[] imgWebp = null;

        BufferedImage image = null;
        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));
            image = ImageIO.read(new ByteArrayInputStream(input));
            ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();

            WebPWriteParam writeParam = new WebPWriteParam(writer.getLocale());
            writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            writeParam.setCompressionType(writeParam.getCompressionTypes()[WebPWriteParam.LOSSY_COMPRESSION]);
            writeParam.setCompressionQuality(quality);

            // Save the image
            writer.setOutput(new FileImageOutputStream(new File(pathWebP.toString())));
            writer.write(null, new IIOImage(image, null, null), writeParam);

            byte[] temp = Files.readAllBytes(pathWebP);
            imgWebp = Arrays.copyOf(temp, temp.length);
            if (deleteLocalImg) {
                boolean statusDelete = new File(pathWebP.toString()).delete();
                System.out.println(statusDelete);
            }

        } catch (IOException e) {
            e.printStackTrace();
            new EncoderException("file "+ fileName +" did not encode to " + formatName, e);
        }

        return imgWebp;
    }

    @Override
    public String toWebP(Path input) throws EncoderException {
        String formatName = "webp";

        String pathWebP = Utils.removeExtention(input.toString()).concat(".webp");

        BufferedImage image = null;
        try {
            Files.createDirectories(Paths.get(TEMP_DIR).toAbsolutePath().normalize());
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));
            image = ImageIO.read(new File(input.toString()));
            boolean statusWrite = ImageIO.write(image, formatName, new File(pathWebP));
        } catch (IOException e) {
            e.printStackTrace();
            throw new EncoderException("file did not encode to " + formatName, e);
        }
        return pathWebP;
    }

    @Override
    public BufferedImage toWebP(BufferedImage input, String dirName, String fileName, boolean deleteLocalImg){
        String formatName = "webp";
        Path pathTempImg = Paths.get(TEMP_DIR + dirName + fileName + "." + formatName).toAbsolutePath().normalize();


        BufferedImage imageWebp = null;
        try {
            Files.createDirectories(pathTempImg);
            FileUtils.cleanDirectory(new File(Paths.get(TEMP_DIR)
                    .toAbsolutePath().normalize().toString()));
            boolean statusWrite = ImageIO.write(input, formatName, new File(pathTempImg.toString()));
            imageWebp = ImageIO.read(new File(pathTempImg.toString()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new EncoderException("file did not encode to " + formatName, e);
        }
        return imageWebp;
    }

    @Override
    public List<String> toWebP(List<Path> input) throws EncoderException {
        String formatName = "webp";
        List<String> convertImages = new ArrayList<>();
        Map<Path, String> inout = new HashMap<>();

        input.forEach(p -> {
            convertImages.add(Utils.removeExtention(p.toString()).concat(".webp"));
            inout.put(p, Utils.removeExtention(p.toString()).concat(".webp"));
        });

        for(Map.Entry<Path, String> entry : inout.entrySet()){
            BufferedImage image = null;
            try {
                image = ImageIO.read(new File(entry.getKey().toString()));
                boolean statusWrite = ImageIO.write(image, formatName, new File(entry.getValue()));
            } catch (IOException e) {
                e.printStackTrace();
                throw new EncoderException("file did not encode to " + formatName, e);
            }
        }
        return inout.values().stream().collect(Collectors.toList());
    }

    @Override
    public String toWebP(String input) throws EncoderException {
        String formatName = "webp";

        String pathWebP = Utils.removeExtention(input).concat(".webp");

        try {
            BufferedImage image = ImageIO.read(new File(input));
            boolean statusWrite = ImageIO.write(image, formatName, new File(pathWebP));
        } catch (IOException e) {
            e.printStackTrace();
            throw new EncoderException("file did not encode to " + formatName, e);
        }
        return pathWebP;
    }
}
