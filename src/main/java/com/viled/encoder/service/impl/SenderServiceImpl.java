package com.viled.encoder.service.impl;

import com.viled.encoder.config.ViledProperties;
import com.viled.encoder.exception.SenderException;
import com.viled.encoder.service.SenderService;
import com.viled.encoder.utils.Utils;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.channels.Channels;
import java.nio.channels.Pipe;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SenderServiceImpl implements SenderService {
    private final String serverAndpoint;
    private final String nameFieldFile;
    private final String nameFieldItem;

    @Autowired
    public SenderServiceImpl(ViledProperties senderProperties) {
        this.serverAndpoint = senderProperties.getServerAndPoint();
        this.nameFieldFile = senderProperties.getNameFieldFile();
        this.nameFieldItem = senderProperties.getNameFieldItem();

        try {
            //TODO проверка готовности сервера
        } catch (Exception ex) {
            throw new SenderException("Server not found", ex);
        }
    }

    @SneakyThrows
    @Override
    public HttpResponse<String> send(HttpEntity httpEntity) throws SenderException {
        Pipe pipe = Pipe.open();
        HttpResponse<String> responseBody;
        HttpClient httpClient = HttpClient.newHttpClient();

        try {
            new Thread(() -> {
                try (OutputStream outputStream = Channels.newOutputStream(pipe.sink())) {
                    httpEntity.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            HttpRequest request = HttpRequest.newBuilder(new URI(serverAndpoint))
                    .header("Content-Type", httpEntity.getContentType().getValue())
                    .POST(HttpRequest.BodyPublishers.ofInputStream(() -> Channels.newInputStream(pipe.source())))
                    .build();

            responseBody = httpClient.send(request, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
            if (responseBody.statusCode() != 200){
                throw new SenderException("Server did not accept files\n" + responseBody.body());
            }

            return  responseBody;

        } catch (IOException | URISyntaxException | InterruptedException e) {
            e.printStackTrace();
            throw new SenderException("Did not read files\n", e);
        }
    }

    @Override
    public HttpEntity create(Path pathFile, Integer itemId) {
        String name = pathFile.getFileName().toString();
        ContentType contentType = ContentType.IMAGE_WEBP;

        return MultipartEntityBuilder.create()
                .addPart(nameFieldItem,
                        new StringBody(itemId.toString(),
                        ContentType.create("application/x-www-form-urlencoded",
                                StandardCharsets.UTF_8)))
                .addBinaryBody(nameFieldFile, new File(pathFile.toString()), contentType, name)
                .build();
    }

    @Override
    public HttpEntity create(List<Path> pathFile, Integer itemId) {
        ContentType contentType = ContentType.IMAGE_WEBP;

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart(nameFieldItem,
                new StringBody(itemId.toString(),
                        ContentType.create("application/x-www-form-urlencoded",
                                StandardCharsets.UTF_8)));

        pathFile.forEach(p -> builder.addBinaryBody(nameFieldFile,
                new File(p.toString()), contentType,
                FilenameUtils.getName(p.toString())));

        return builder.build();
    }
}
