package com.viled.encoder.service.impl;

import com.viled.encoder.config.FileStorageProperties;
import com.viled.encoder.exception.FileStorageException;
import com.viled.encoder.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {
    private final Path fileStorageLocation;

    @Autowired
    public StorageServiceImpl(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public String saveFileLocal(MultipartFile file) throws FileStorageException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence: " + fileName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return targetLocation.toString();

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
        }
    }

    @Override
    public String saveFileLocal(String imgName, String dirName, byte[] data, String type) throws FileStorageException {
        Path filePath = Paths.get(fileStorageLocation.toString(), dirName, imgName );

        try {
            Files.createDirectories(Paths.get(fileStorageLocation.toString(), dirName).normalize());
            if(imgName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence: " + imgName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(filePath.toString() + "." + type);
            Files.copy(new ByteArrayInputStream(data),  targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return targetLocation.toString();

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + imgName + ". Please try again!", ex);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileStorageException("Could not store file " + imgName + ". Please try again!", e);
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new FileStorageException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File not found " + fileName, ex);
        }
    }

    @Override
    public List<Path> saveFilesLocal(List<MultipartFile> files) throws IOException {
        List<Path> imagesPath = new ArrayList<>();

        for (MultipartFile file : files) {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            if (file.isEmpty()) {
                continue;
            }

            byte[] bytes = file.getBytes();
            Path path = this.fileStorageLocation.resolve(fileName);
            System.out.println(path);
            Files.write(path, bytes);
            imagesPath.add(path);
        }
        return  imagesPath;
    }
}
