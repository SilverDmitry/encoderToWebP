package com.viled.encoder.service;

import com.viled.encoder.exception.SenderException;
import org.apache.http.HttpEntity;

import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.util.List;

public interface SenderService {
    HttpResponse<String> send(HttpEntity httpEntity) throws SenderException;
    HttpEntity create(Path pathFile, Integer itemId);
//    HttpEntity create(List<String> pathFile, Integer itemId);
    HttpEntity create(List<Path> pathFile, Integer itemId);
}
