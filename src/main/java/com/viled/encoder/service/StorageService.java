package com.viled.encoder.service;

import com.viled.encoder.exception.FileStorageException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface StorageService {
    public String saveFileLocal(MultipartFile file) throws FileStorageException;
    public Resource loadFileAsResource(String fileName);
    public List<Path> saveFilesLocal(List<MultipartFile> files) throws IOException;
    public String saveFileLocal(String imgName, String dirName, byte[] data, String type) throws FileStorageException;
}
