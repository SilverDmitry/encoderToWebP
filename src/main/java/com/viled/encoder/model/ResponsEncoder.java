package com.viled.encoder.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponsEncoder {
    private String fileInput;
    private String fileOutput;
    private String error;
}
