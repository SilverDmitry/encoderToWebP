package com.viled.encoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.server.SecurityWebFilterChain;

@SpringBootApplication
public class ImageEncoderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageEncoderApplication.class, args);
	}

}
