package com.viled.encoder.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ViledProperties {
    private String serverAndPoint;
    private String nameFieldFile;
    private String nameFieldItem;

    ViledProperties(@Value("${viled.server.andpoint}") String serverAndPoint,
                    @Value("${viled.name-field-file}") String nameFieldFile,
                    @Value("${viled.name-field-item}") String nameFieldItem) {
        this.serverAndPoint = serverAndPoint;
        this.nameFieldFile = nameFieldFile;
        this.nameFieldItem = nameFieldItem;
    }

    public String getServerAndPoint() {
        return serverAndPoint;
    }
    public void setServerAndPoint(String serverAndPoint) {
        this.serverAndPoint = serverAndPoint;
    }

    public String getNameFieldFile() {
        return nameFieldFile;
    }
    public void setNameFieldFile(String nameFieldFile) {
        this.nameFieldFile = nameFieldFile;
    }

    public String getNameFieldItem() {
        return nameFieldItem;
    }
    public void setNameFieldItem(String nameFieldItem) {
        this.nameFieldItem = nameFieldItem;
    }
}
