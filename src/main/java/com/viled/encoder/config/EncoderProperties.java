package com.viled.encoder.config;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class EncoderProperties {
    private String tempDir;
    private float QUALITY;

    EncoderProperties(@Value("${encoder.quality}") Float quality,
                      @Value("${file.temp-dir}") String tempDir){
        this.tempDir = tempDir;
        this.QUALITY = quality;
    }}
