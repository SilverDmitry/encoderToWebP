package com.viled.encoder.exception;

public class EncoderException extends RuntimeException {
    public EncoderException(String message) {
        super(message);
    }
    public EncoderException(String message, Throwable cause) {
        super(message, cause);
    }
}
