package com.viled.encoder.controller;

import com.viled.encoder.exception.EncoderException;
import com.viled.encoder.exception.FileStorageException;
import com.viled.encoder.exception.SenderException;
import com.viled.encoder.service.EncoderService;
import com.viled.encoder.service.SenderService;
import com.viled.encoder.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/encoderImages")
public class EncoderRESTController {
    private final StorageService storageService;
    private final EncoderService encoderService;
    private final SenderService senderService;
    private String serverEndPoint;

    @Autowired
    public EncoderRESTController(StorageService storageService,
                                 EncoderService encoderService,
                                 SenderService senderService,
                                 @Value("${viled.server.andpoint}") String serverEndPoint) {
        this.storageService = storageService;
        this.encoderService = encoderService;
        this.senderService = senderService;
        this.serverEndPoint = serverEndPoint;
    }

    @GetMapping("/hello")
    public String hello() {
        return String.format("Hello in Encoder Image To WebP");
    }

    @PostMapping("/image/upload")
    public ResponseEntity uploadFiles(
            @RequestParam("item_id") Integer itemId,
            @RequestParam("file") MultipartFile[] files) {

        if (files.length == 0) {
            return new ResponseEntity("please select a file!", HttpStatus.BAD_REQUEST);
        }

        for(int i=0; i< files.length; i++) {
            if (!checkContentType(files[i].getContentType())) {
                return new ResponseEntity("Failed format file ! It can be .jpeg, .jpg, .png ", HttpStatus.BAD_REQUEST);
            }
            if (files[i].getName().contains("..")) {
                return new ResponseEntity("Filename contains invalid format .. ", HttpStatus.BAD_REQUEST);
            }
        }

        try {
            List<Path> pathEncodedFiles = encoderService.execute(files, "webp");
            senderService.send(senderService.create(pathEncodedFiles, itemId));

            return new ResponseEntity("Successfully encoded!\n " +
                    "\nFiles encoded: \n"
                    + Arrays.stream(files).map(x -> x.getOriginalFilename())
                    .filter(x -> !StringUtils.isEmpty(x))
                    .collect(Collectors.joining("\n"))
                    + "\n\nItem id: " + itemId
                    + "\n\nSent to the server: " + serverEndPoint,
                    new HttpHeaders(), HttpStatus.OK);

        } catch (EncoderException e) {
            return new ResponseEntity("This file not encoding" + e, HttpStatus.BAD_REQUEST);
        } catch (SenderException e) {
            return new ResponseEntity("This file not send to server\n" + e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/test")
    public ResponseEntity test(
            @RequestParam("item_id") Integer itemId,
            @RequestParam("file") MultipartFile[] uploadfile) {

        System.out.println("принимаем фаил");
        System.out.println("ID = " + itemId);
        System.out.println("File " + uploadfile);
        Arrays.stream(uploadfile).forEach(System.out::println);

        return new ResponseEntity("Successfully uploaded - ", new HttpHeaders(), HttpStatus.OK);

    }

    private boolean checkContentType(String contentType) {
        return !StringUtils.isEmpty(contentType)
                || !(contentType.equals("image/gif")
                || contentType.equals("image/png")
                || contentType.equals("image/jpg")
                || contentType.equals("image/jpeg")
        );
    }

}
