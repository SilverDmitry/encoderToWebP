FROM eclipse-temurin:17-jdk-jammy
VOLUME /storage/images/uploads
COPY target/ app
ENTRYPOINT ["java","-jar","app/image-encoder-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080